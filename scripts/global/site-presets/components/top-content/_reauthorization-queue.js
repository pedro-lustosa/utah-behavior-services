/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { HeightSlider1 } from "../../../../global/site-presets/referrers/constructors/_height-slider-1.js";

/* Identificadores */

export const reauthorizationQueue = document.getElementById( "reauthorization-queue" );

reauthorizationQueue:
  { var boxContainer = reauthorizationQueue.box = reauthorizationQueue.querySelector( ".box-container" );
    boxContainer:
      { var heading = boxContainer.heading = boxContainer.querySelector( ".heading-container" ),
            contentBody = boxContainer.body = boxContainer.querySelector( ".content-body" );
        heading:
          { var headingArrow = heading.arrow = heading.querySelector( ".arrow-container" ) }
        contentBody:
          { var queueList = contentBody.queueList = reauthorizationQueue.querySelector( ".queue-list" ) } } }

/* Instanciações */

HeightSlider1:
  { new HeightSlider1( contentBody, boxContainer, headingArrow, { overflow: true } ) }
