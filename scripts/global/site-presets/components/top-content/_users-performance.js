/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { HeightSlider1 } from "../../../../global/site-presets/referrers/constructors/_height-slider-1.js";

import { BoxesList1 } from "../../../../global/site-presets/referrers/constructors/_boxes-list-1.js";

import { RemoveButton1 } from "../../../../global/site-presets/referrers/constructors/_remove-button-1.js";

/* Identificadores */

export const usersPerformance = document.getElementById( "users-performance" );

usersPerformance:
  { var usersList = usersPerformance.usersList = usersPerformance.querySelector( ".users-list" );
    usersList:
      { var boxesContainers = usersList.boxesContainers = new oElementsArray( usersList.getElementsByClassName( "box-container" ) );
        boxesContainers:
          { var contentHeadings = boxesContainers.headings = new oElementsArray( boxesContainers.oSelectForEach( ".heading-container" ) ),
                contentBodies = boxesContainers.bodies = new oElementsArray( boxesContainers.oSelectForEach( ".content-body" ) );
            contentHeadings:
              { var headingsArrows = contentHeadings.oSelectForEach( ".arrow-container" ) }
            contentBodies:
              { var contentFooters = contentBodies.footers = new oElementsArray( contentBodies.oSelectForEach( ".additional-info" ) );
                contentFooters:
                  { var expandOptions = contentFooters.expandOptions = new oElementsArray( contentFooters.oSelectForEach( ".expand-option" ) ),
                        hiddenFields = contentFooters.hiddenFields = new oElementsArray( contentFooters.oSelectForEach( ".hidden-fields" ) );
                    expandOptions:
                      { var footersArrows = expandOptions.arrows = expandOptions.oSelectForEach( ".arrow-container" ) }
                    hiddenFields:
                      { var userReports = hiddenFields.userReports = new oElementsArray( hiddenFields.oSelectForEach( ".user-reports" ) );
                        userReports:
                          { var boxesList = userReports.boxesList = new oElementsArray( userReports.oSelectForEach( ".boxes-list" ) );
                            boxesList:
                              { var reportBoxesListings = boxesList.items = new oElementsArray( boxesList.oSelectForEach( "li" ) ),
                                    reportBoxes = boxesList.reportBoxes = new oElementsArray( boxesList.oSelectForEach( ".report-box", "children", Infinity ) );
                                reportBoxes:
                                  { var reportBoxesSet = reportBoxes.set = reportBoxes.oSegmentByParent( boxesList ),
                                        removeButtons = reportBoxes.removeButtons = new oElementsArray( reportBoxes.oSelectForEach( ".options-list .close" ) );
                                    removeButtons:
                                      { var removeButtonsSet = removeButtons.set = removeButtons.oSegmentByParent( boxesList ) } } } } } } } } } }

/* Instanciações */

HeightSlider1:
  { for( let i = 0; i < boxesContainers.length; i++ )
      { new HeightSlider1( contentBodies[i], boxesContainers[i], headingsArrows[i], { shrinkSlidedChild: { slidedChild: hiddenFields[i], referenceElement: contentBodies[i] } } );
        new HeightSlider1( hiddenFields[i], contentBodies[i], footersArrows[i], { expandSlidedParent: contentBodies[i] } ) } }

BoxesList1:
  { let userReportsI = userReports.values(),
        reportBoxesSetI = reportBoxesSet.values(),
        removeButtonsSetI = removeButtonsSet.values();
    for( let i = 0; i < hiddenFields.length; i++ )
      { if( !hiddenFields[i].oBringChildren( userReports, 1 ) ) continue;
        new BoxesList1( userReportsI.next().value, hiddenFields[i], reportBoxesSetI.next().value, removeButtonsSetI.next().value ) } }

RemoveButton1:
  { for( let i = 0; i < removeButtons.length; i++ ) new RemoveButton1( removeButtons[i], reportBoxes[i].closest( "li" ), { fade: true } ) }

/* Eventos */

shrinkContentBodiesHeight:
  { for( let list of boxesList )
      { list.addEventListener( "transitionend", shrinkContentBodyHeight ) } }

/* Funções */

function shrinkContentBodyHeight( event )
  { let currentList = event.currentTarget;
    if( event.target.tagName != "LI" || event.propertyName != "opacity" || currentList.children.length ) return;
    let contentBody = currentList.oBringParents( contentBodies, 1 ),
        expandOption = contentBody.oBringChildren( expandOptions, 1 ),
        hiddenField = contentBody.oBringChildren( hiddenFields, 1 );
    contentBody.style.height = ( contentBody.offsetHeight - hiddenField.offsetHeight ) / 10 + "rem";
    contentBody.classList.remove( "extended" );
    disableExpandOption( expandOption ) }

function disableExpandOption( expandOption )
  { expandOption.classList.add( "disabled" );
    expandOption.oBringChildren( footersArrows, 1 ).oDisableEventType( "click" ) }

/* Instruções Iniciais */

expandOptions.oSowClass( "disabled", element => !element.oBringParents( contentBodies, 1 ).oBringChildren( hiddenFields, 1 ).scrollHeight );
