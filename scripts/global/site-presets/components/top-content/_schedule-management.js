/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { Table1 } from "../../../../global/site-presets/referrers/constructors/_table1.js";

/* Identificadores */

export const scheduleManagement = document.getElementById( "schedule-management" );

scheduleManagement:
  { var scheduleTable = scheduleManagement.table = scheduleManagement.querySelector( ".schedule-table" );
    scheduleTable:
      { var tableHead = scheduleTable.head = scheduleTable.tHead,
            tableBody = scheduleTable.body = scheduleTable.tBodies[0],
            tableRows = Array.from( scheduleTable.rows );
        tableBody:
          { var tableRows = tableBody.tr = new oElementsArray( tableBody.rows );
            tableRows:
              { var tableCells = tableRows.cells = new oElementsArray( tableRows.oSelectForEach( "td", "children", Infinity, 1 ) );
                tableCells:
                  { var tableCellsGroup = tableCells.group = tableCells.oSegmentByParent( tableRows ) } } } } }

/* Instanciações */

Table1:
  { new Table1( scheduleTable, tableRows, tableCells, tableCellsGroup ) }
