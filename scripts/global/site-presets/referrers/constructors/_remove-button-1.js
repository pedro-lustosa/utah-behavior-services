/*---- Remove Button 1 ----*/

// Construtor

export const RemoveButton1 = function( button, removedContent, featuresMap = { softRemoval: false, fade: false } )
  { assignReferrers:
      { this.button = button; this.removedContent = removedContent; this.featuresMap = featuresMap }
    assignEvents:
      { let removeContent = this.removeContent.bind( this, removedContent, featuresMap.softRemoval ),
            fadeContent = this.fadeContent.bind( this, removedContent );
        if( featuresMap.fade )
          { button.addEventListener( "click", fadeContent );
            removedContent.addEventListener( "transitionend", removeContent ) }
        else
          { button.addEventListener( "click", removeContent ) } } }

// Chaves do Protótipo

prototypeKeys:
  { let prototype = RemoveButton1.prototype;
    contentRemoval:
      { prototype.fadeContent = function ( removedContent, event )
          { removedContent.style.opacity = "0" }
        prototype.removeContent = function ( removedContent, softRemoval = false, event )
          { if( event.type == "transitionend" && ( event.target != event.currentTarget || event.propertyName != "opacity" || window.getComputedStyle( event.target ).opacity != "0" ) ) return;
            softRemoval ? removedContent.style.display = "none" : removedContent.remove() } } }
