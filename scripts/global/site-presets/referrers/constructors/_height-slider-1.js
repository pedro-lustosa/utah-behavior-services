/*---- Height Slider 1 ----*/

// Construtor

export const HeightSlider1 = function( slidedElement, referenceElement, trigger,
                                       featuresMap = { overflow: false, adjacentElement: false, expandSlidedParent: false, shrinkSlidedChild: false } )
  { assignReferrers:
      { this.slidedElement = slidedElement; this.referenceElement = referenceElement; this.trigger = trigger; this.featuresMap = featuresMap }
    assignCoreEvents:
      { let slide = this.slide.bind( this, slidedElement, referenceElement, trigger ),
            renableClicksInTrigger = this.renableClicksInTrigger.bind( this, trigger ),
            readjustSlidedElementHeight = this.readjustSlidedElementHeight.bind( this, slidedElement, referenceElement );
        trigger.addEventListener( "click", slide );
        trigger.addEventListener( "transitionend", renableClicksInTrigger );
        window.addEventListener( "resize", readjustSlidedElementHeight ) }
    featuresMapAddOns:
      { expandSlidedParent:
          { if( !featuresMap.expandSlidedParent ) break expandSlidedParent;
            var slidedParent = featuresMap.expandSlidedParent;
            let updateParentHeight = this.updateParentHeight.bind( this, slidedElement, referenceElement, slidedParent );
            trigger.addEventListener( "click", updateParentHeight ) }
        shrinkSlidedChild:
          { if( !featuresMap.shrinkSlidedChild ) break shrinkSlidedChild;
            var slidedChild = featuresMap.shrinkSlidedChild.slidedChild,
                slidedChildReferenceElement = featuresMap.shrinkSlidedChild.referenceElement;
            let updateChildHeight = this.updateChildHeight.bind( this, slidedChild, slidedChildReferenceElement );
            trigger.addEventListener( "click", updateChildHeight ) }
        adjacentElement:
          { if( featuresMap.adjacentElement ) var adjacentElement = this.adjacentElement = featuresMap.adjacentElement }
        overflow:
          { if( !featuresMap.overflow ) break overflow;
            let triggerHiddenOverflow = this.triggerHiddenOverflow.bind( this, slidedElement, adjacentElement ),
                triggerVisibleOverflow = this.triggerVisibleOverflow.bind( this, slidedElement, referenceElement, adjacentElement );
            trigger.addEventListener( "click", triggerHiddenOverflow );
            slidedElement.addEventListener( "transitionend", triggerVisibleOverflow ) } } }

// Chaves do Protótipo

prototypeKeys:
  { let prototype = HeightSlider1.prototype;
    slideTargetElement:
      { prototype.slide = function( slidedElement, referenceElement, trigger, event )
          { if( trigger.closest( ".disabled" ) ) return;
            trigger.oDisableEventType( "click" );
            slidedElement.style.height = referenceElement.classList.toggle( "extended" ) ? slidedElement.scrollHeight / 10 + "rem" : "0" }
        prototype.readjustSlidedElementHeight = function( slidedElement, referenceElement, event )
          { if( !referenceElement.classList.contains( "extended" ) ) return;
            let actualHeight = slidedElement.oGetActualSize( { height: true } );
            if( slidedElement.offsetHeight != actualHeight ) slidedElement.oAdjustSize( { height: actualHeight } ) }
        prototype.renableClicksInTrigger = function( trigger, event )
          { if( !trigger.closest( ".disabled" ) ) trigger.oRenableEventType( "click" ) } }
    overflowAdjustments:
      { prototype.triggerHiddenOverflow = function( slidedElement, adjacentElement, event )
          { let targetElements = [].concat( slidedElement, adjacentElement || [] );
            for( let element of targetElements ) element.style.overflow = "hidden" }
        prototype.triggerVisibleOverflow = function( slidedElement, referenceElement, adjacentElement, event )
          { if( adjacentElement ) adjacentElement.style.overflow = "visible";
            if( referenceElement.classList.contains( "extended" ) ) slidedElement.style.overflow = "visible" } }
    adjustSlidedParentHeight:
      { prototype.updateParentHeight = function( slidedElement, referenceElement, slidedParent, event )
          { slidedParent.style.height = ( referenceElement.classList.contains( "extended" ) ?
              slidedParent.offsetHeight + slidedElement.scrollHeight : slidedParent.offsetHeight - slidedElement.scrollHeight ) / 10 + "rem" } }
    adjustSlidedChildHeight:
      { prototype.updateChildHeight = function( slidedChild, referenceElement, event )
          { if( !referenceElement.classList.contains( "extended" ) ) return;
            slidedChild.style.height = "0"; referenceElement.classList.remove( "extended" ) } } }
