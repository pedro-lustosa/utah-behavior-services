/*---- Modal Box 1 ----*/

// Construtores

import { RemoveButton1 } from "../../../../global/site-presets/referrers/constructors/_remove-button-1.js";

// Construtor

export const ModalBox1 = function( modalContainer, modal, triggers, exitButton, featuresMap = { hideBody: false } )
  { assignReferrers:
      { this.modalContainer = modalContainer; this.modal = modal; this.triggers = triggers; this.exitButton = exitButton; this.featuresMap = featuresMap;
        featuresMap:
          { var hideBody = featuresMap.hideBody } }
    assignConstructors:
      { new RemoveButton1( exitButton, modal, { softRemoval: true, fade: true } ) }
    assignEvents:
      { let revealModalContainer = this.revealModalContainer.bind( this, modalContainer, hideBody ),
            revealModal = this.revealModal.bind( this, modalContainer, modal ),
            hiddenModalContainer = this.hiddenModalContainer.bind( this, modalContainer, modal ),
            resetModalContainer = this.resetModalContainer.bind( this, modalContainer, modal, hideBody ),
            revealModalContainerOnTouchTime = this.revealModalContainerOnTouchTime.bind( this, modalContainer, hideBody );
        for( let trigger of triggers )
          { trigger.addEventListener( "touchstart", revealModalContainerOnTouchTime );
            trigger.addEventListener( "dblclick", revealModalContainer ) };
        for( let _event of [ revealModal, resetModalContainer ] ) modalContainer.addEventListener( "transitionend", _event );
        modal.addEventListener( "transitionend", hiddenModalContainer ) } }

// Chaves do Protótipo

prototypeKeys:
  { let prototype = ModalBox1.prototype;
    transitionControl:
      { prototype.revealModalContainerOnTouchTime = function( modalContainer, hideBody, event )
          { let triggerTime = 600,
                revealModalContainer = window.setTimeout( prototype.revealModalContainer, triggerTime, modalContainer, hideBody );
            event.target.addEventListener( "touchend", () => clearTimeout( revealModalContainer ), { once: true } );
            event.preventDefault() }
        prototype.revealModalContainer = function( modalContainer, hideBody, event )
          { modalContainer.style.visibility = "visible"; modalContainer.style.opacity = "1";
            if( hideBody ) document.body.style.overflow = "hidden" }
        prototype.revealModal = function( modalContainer, modal, event )
          { if( modalContainer != event.target || event.propertyName != "opacity" || window.getComputedStyle( modalContainer ).opacity != "1" ) return;
            modal.style.visibility = "visible"; modal.style.opacity = "1" }
        prototype.hiddenModalContainer = function( modalContainer, modal, event )
          { if( modal != event.target || event.propertyName != "opacity" || window.getComputedStyle( modal ).opacity != "0" ) return;
            modalContainer.style.opacity = "0" }
        prototype.resetModalContainer = function( modalContainer, modal, hideBody, event )
          { if( modalContainer != event.target || event.propertyName != "opacity" || window.getComputedStyle( modalContainer ).opacity != "0" ) return;
            modalContainer.style.visibility = "hidden";
            modal.style.visibility = "hidden"; modal.style.display = "";
            if( hideBody ) document.body.style.overflow = "" } } }
