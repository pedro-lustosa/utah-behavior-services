/*---- Boxes List 1 ----*/

// Construtor

export const BoxesList1 = function( boxesSection, sectionContainer, boxes, removeButtons, featuresMap = { squaresList: false } )
  { assignReferrers:
      { this.boxesSection = boxesSection; this.sectionContainer = sectionContainer; this.boxes = boxes; this.removeButtons = removeButtons; this.featuresMap = featuresMap;
        featuresMap:
          { var squaresList = featuresMap.squaresList;
            squaresList:
              { if( !squaresList ) break squaresList;
                var activeSquares = squaresList.filter( square => square.classList.contains( "new" ) ) } } }
    assignEvents:
      { removeButtons:
          { for( let i = 0; i < removeButtons.length; i++ )
              { let handlersArray = [];
                coreEvents:
                  { let adjustDistance = this.adjustDistance.bind( this, boxesSection, sectionContainer, boxes, boxes[i] );
                    handlersArray.push( adjustDistance ) }
                squaresList:
                  { if( !squaresList ) break squaresList;
                    let defaultSquare = this.defaultSquare.bind( this, activeSquares[i] );
                    handlersArray.push( defaultSquare ) }
                for( let handler of handlersArray ) removeButtons[i].addEventListener( "click", handler ) } } } }

// Chaves do Protótipo

prototypeKeys:
  { let prototype = BoxesList1.prototype;
    distanceAdjustments:
      { prototype.adjustDistance = function( boxesSection, sectionContainer, boxes, targetBox, event )
          { if( boxes.some( box => !box.isSameNode( targetBox ) && boxesSection.contains( box ) ) ) return;
            targetBox.closest( "li" ).addEventListener( "transitionend", revalueBoxesSectionHeight );
            function revalueBoxesSectionHeight( event )
              { if( event.target != event.currentTarget || event.propertyName != "opacity" ) return;
                boxesSection.style.display = "none";
                sectionContainer.oAdjustSize( { height: sectionContainer.oGetActualSize( { height: true } ) } );
                this.removeEventListener( "transitionend", revalueBoxesSectionHeight ) } } }
    squaresList:
      { prototype.defaultSquare = function( targetSquare, event )
          { targetSquare.classList.remove( "new" ); targetSquare.style.backgroundColor = "transparent" } } }
