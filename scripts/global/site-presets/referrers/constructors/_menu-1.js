/*---- Menu 1 ----*/

// Construtor

export const Menu1 = function( self, items, arrows, nextArrow, previousArrow )
  { assignReferrers:
      { this.self = self; this.items = items, this.arrows = arrows;
        arrows:
          { arrows.next = nextArrow; arrows.previous = previousArrow } }
    assignEvents:
      { for( let item of items ) item.addEventListener( "click", event => this.changeActiveMenuItem( event ) );
        for( let arrow of [ nextArrow, previousArrow ] ) arrow.addEventListener( "click", () => this.changeDisplayedMenuItem( arrow, arrow == nextArrow ? +1 : -1 ) );
        window.addEventListener( "resize", event => this.updateArrowsStyle( event ) ) }
    initialInstructions:
      { this.updateArrowsStyle() } }

// Inquéritos de Mídia

export const Menu1Media = { maxWidth: [ 599 ] };

// Chaves do Protótipo

prototypeKeys:
  { let prototype = Menu1.prototype;
    changeMenuStyle:
      { prototype.changeActiveMenuItem = function ( event )
          { let item = event.currentTarget;
            item.oRestrictClass( "current", this.items ) }
        prototype.updateArrowsStyle = function ( event )
          { if( window.innerWidth > Menu1Media.maxWidth[ 0 ] && event && event.type == "resize" ) return;
            let items = this.items,
                currentMenuItem = items.find( item => item.classList.contains( "current" ) ),
                previousArrow = this.arrows.previous,
                nextArrow = this.arrows.next;
            nextArrow.classList.toggle( "clickable", items[ items.indexOf( currentMenuItem ) + 1 ] != undefined );
            previousArrow.classList.toggle( "clickable", items[ items.indexOf( currentMenuItem ) - 1 ] != undefined ) } }
    changeDisplayedMenuItem:
      { prototype.changeDisplayedMenuItem = function ( arrow, direction )
          { let oppositeArrow = this.arrows.find( _arrow => _arrow != arrow ),
                items = this.items,
                currentIndex = items.findIndex( item => item.classList.contains( "current" ) ),
                newItem;
            if( ( newItem = items[ currentIndex + direction ] ) == undefined ) return;
            newItem.oRestrictClass( "current", items );
            this.updateArrowsStyle() } } }
