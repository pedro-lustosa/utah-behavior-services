/*---- Table 1 ----*/

// Construtor

export const Table1 = function( table, rows, allCells, cellsSet )
  { assignReferrers:
      { this.table = table; this.rows = rows; this.cells = { all: allCells, set: cellsSet } }
    assignEvents:
      {  }
    initialInstructions:
      { this.hiddenFilledBorders( cellsSet ) } }

// Chaves do Protótipo

prototypeKeys:
  { let prototype = Table1.prototype;
    filledCells:
      { prototype.hiddenFilledBorders = function( cellsSet )
          { cellsSet.forEach( ( set, index, array ) =>
              { for( let i = 0; i < set.length; i++ )
                  { let cell = set[i],
                        nextSet = array[ index + 1 ];
                    if( !nextSet ) continue;
                    if( !cell.classList.contains( "filled" ) ) { cell.style.borderBottomColor = ""; continue };
                    let nextCell = nextSet[i];
                    if( nextCell.classList.contains( "filled" ) && !nextCell.innerText ) cell.style.borderBottomColor = "transparent" } } ) } } }
