/*---- Editable Content 1 ----*/

// Construtor

export const EditableContent1 = function( sectionContainer, buttonsSet = { edit: false, cancel: false, save: false }, editableData, dataContainers,
                                          featuresMap = { setWidth: false } )
  { assignReferrers:
      { this.sectionContainer = sectionContainer; this.buttonsSet = buttonsSet; this.editableData; this.dataContainers = dataContainers; this.featuresMap = featuresMap;
        buttonsSet:
          { var editButton = buttonsSet.edit, cancelButton = buttonsSet.cancel, saveButton = buttonsSet.save }
        data:
          { var generalData = [], inputData = [];
            for( let data of editableData ) data.tagName == "INPUT" ? inputData.push( data ): generalData.push( data ) } }
    assignEvents:
      { buttonsSet:
          { let activeEditMode = this.activeEditMode.bind( this, editButton, sectionContainer, editableData, dataContainers, inputData ),
                undoEditMode = this.undoEditMode.bind( this, cancelButton, sectionContainer, editableData, generalData, inputData ),
                finishEditMode = this.finishEditMode.bind( this, saveButton, sectionContainer, editableData, inputData ),
                buttonsMap = new Map( [ [ editButton, activeEditMode ], [ cancelButton, undoEditMode ], [ saveButton, finishEditMode ] ] );
            for( let [ button, handler ] of buttonsMap ) button.addEventListener( "click", handler ) }
        editableData:
          { for( let data of editableData )
              { let saveOriginalText = this.saveOriginalText.bind( this, data ),
                    disableLineBreak = this.disableLineBreak.bind( this, data ),
                    changeFocus = this.changeFocus.bind( this, data, editableData ),
                    eventsMap = new Map( [ [ saveOriginalText, "focus" ], [ disableLineBreak, "keypress" ], [ changeFocus, "keypress" ] ] );
                for( let [ handler, _event ] of eventsMap ) data.addEventListener( _event, handler ) } }
        featuresMap:
          { setWidth:
              { if( !featuresMap.setWidth ) break setWidth;
                let resizeDataWidth = this.resizeDataWidth.bind( this, sectionContainer, editableData, dataContainers );
                window.addEventListener( "resize", resizeDataWidth ) } } } }

// Chaves do Protótipo

prototypeKeys:
  { let prototype = EditableContent1.prototype;
    activeEditMode:
      { prototype.activeEditMode = function( editButton, sectionContainer, editableData, dataContainers, inputData, event )
          { for( let data of editableData ) inputData.includes( data ) ? data.removeAttribute( "disabled" ) : data.setAttribute( "contenteditable", "" );
            sectionContainer.classList.add( "edit-mode" );
            if( this.featuresMap.setWidth ) this.resizeDataWidth( sectionContainer, editableData, dataContainers ) } }
    changeContent:
      { prototype.saveOriginalText = function( data, event )
          { if( "formerText" in data || !data.hasAttribute( "contenteditable" ) ) return;
            data.formerText = data.innerHTML }
        prototype.disableLineBreak = function( data, event )
          { if( event.key != "Enter" || event.currentTarget.classList.contains( "text-set" ) ) return;
            event.preventDefault() }
        prototype.changeFocus = function( data, editableData, event )
          { if( event.key != "Enter" || ( event.currentTarget.classList.contains( "text-set" ) && !event.shiftKey ) ) return;
            if( event.currentTarget.classList.contains( "text-set" ) ) event.preventDefault();
            event.shiftKey ? editableData.oCycle( editableData.indexOf( data ) - 1 ).focus() : editableData.oCycle( editableData.indexOf( data ) + 1 ).focus() } }
    undoEditMode:
      { prototype.undoEditMode = function( cancelButton, sectionContainer, editableData, generalData, inputData, event )
          { for( let data of generalData ) if( data.formerText && data.formerText != data.innerHTML ) data.innerHTML = data.formerText;
            return this.finishEditMode( this.buttonsSet.saveButton, sectionContainer, editableData, inputData ) } }
    finishEditMode:
      { prototype.finishEditMode = function( saveButton, sectionContainer, editableData, inputData, event )
          { for( let data of editableData )
              { if( "formerText" in data ) delete data.formerText;
                data.style.width = "";
                inputData.includes( data ) ? data.setAttribute( "disabled", "" ) : data.removeAttribute( "contenteditable", "" ) }
            sectionContainer.classList.remove( "edit-mode" ) } }
    adjustDataSize:
      { prototype.resizeDataWidth = function( sectionContainer, editableData, dataContainers, event )
          { if( !sectionContainer.classList.contains( "edit-mode" ) ) return;
            for( let data of editableData )
              { if( data.classList.contains( "text-set" ) ) continue;
                let containerWidth = window.getComputedStyle( data.oBringParents( dataContainers, 1 ) ).width.replace( /[^0-9.+-]/g, "" );
                data.style.width = containerWidth / 10 * .98 + "rem" } } } }
