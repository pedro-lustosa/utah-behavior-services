"use strict";

/*---- Funções Auxiliares ----*/

// Funções Auxiliares – Validações

const oAssert = function( expressions, message = "Assertion failed.", errorType = Error ) // Retorna um erro se uma das expressões passadas retornar um valor falso
  { oAssertType( { string: message } );
    if( errorType != Error && ( !errorType[ "prototype" ] || !( errorType[ "prototype" ] instanceof Error ) ) ) throw new TypeError( "Invalid Error object passed." );
    let expressionsList = Array.oMake( expressions );
    for( let expression of expressionsList ) if( !expression ) throw new errorType( message );
    return true }

const oAssertConstructor = function( constructorMap, direct = false ) // Retorna um erro se um dos alvos passados não for uma instância do construtor em suas chaves
  { for( let constructorString in constructorMap )
      { let valuesList = Array.oMake( constructorMap[ constructorString ] ),
            _constructor = eval( constructorString );
        for( let value of valuesList )
          { if( direct && value.constructor != _constructor ) throw new TypeError( `Direct constructor of value ${ value } is ${ value.constructor.name }, but it was expected it to be ${ constructorString }.` )
            else if( !( value instanceof _constructor ) ) throw new TypeError( `Value ${ value } is not an instance of ${ constructorString }.` ) } }
    return true }

const oAssertType = function( typeMap ) // Retorna um erro se o tipo de um dos alvos passados não corresponder com o em suas chaves
  { for( let type in typeMap )
      { let valuesList = Array.oMake( typeMap[ type ] );
        for( let value of valuesList )
          { if( typeof value != type ) throw new TypeError( `Type of value ${ value } is ${ typeof value }, but it was expected it to be ${ type }.` ) } }
    return true }

// Funções Auxiliares – Outros

const oBlockEvent = function( event ) // Bloqueia a propagação de dado evento
  { event.stopPropagation() }

/*---- Arranjos ----*/

// Arranjos – Construtor

Object.defineProperties( Array,
  { oMake: // Converte valores quaisquer em arranjos
      { value: function oMake( target, shallow = true )
          { let array =
              target instanceof Object && !( target instanceof String || target instanceof EventTarget ) && typeof target[ Symbol.iterator ] == "function" ?
                Array.from( target ) : Array.of( target );
            return shallow ? array.slice() : array },
        enumerable: true } } );

// Arranjos – Protótipo

Object.defineProperties( Array.prototype,
  { oCycle: // Quando o número do argumento for negativo ou maior que o tamanho de um arranjo, ajusta aquele de modo a retornar o valor do arranjo que corresponder ao novo valor do número
      { value: function oCycle( integer )
          { oAssertType( { number: integer } );
            if( integer >= this.length ) integer %= this.length
            else while( integer < 0 ) integer += this.length;
            return this[ integer ] },
        enumerable: true },
    oSegment: // Divide o arranjo em sub-arranjos, a começarem com o valor para o qual a função passada retornar um valor verdadeiro
      { value: function oSegment( _function, trim = false )
          { let newArray = [], startIndex = 0;
            this.forEach( ( value, index, array ) =>
              { if( index && _function( value, index, array ) ) { newArray.push( array.slice( startIndex, index ) ); startIndex = index } } );
            newArray.push( this.slice( startIndex ) );
            if( trim && !_function( newArray[0][0] ) ) newArray.shift();
            return newArray },
        enumerable: true } } );

/*---- Elementos ----*/

// Elementos – Protótipo

Object.defineProperties( Element.prototype,
  { oBringChildren: // Retorna um arranjo com os elementos do primeiro argumento descendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringChildren( elementsList = this.querySelectorAll( "*" ), count = Infinity, range = Infinity )
          { oAssertType( { number: [ count, range ] } );
            let actualChildren = this.children, targetChildren = [];
            if( !actualChildren.length || count <= 0 || range <= 0 ) return count == 1 ? undefined : [];
            let arrayList = new oElementsArray( elementsList );
            childrenLoop: for( let child of actualChildren )
              { while( arrayList.includes( child ) )
                  { targetChildren = targetChildren.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( child ) ), 1 ) );
                    if( targetChildren.length == count ) break childrenLoop }
                if( range > 1 && arrayList.length ) targetChildren = targetChildren.concat( child.oBringChildren( arrayList, count - targetChildren.length, range - 1 ) || [] );
                if( targetChildren.length == count ) break }
            return count == 1 ? targetChildren.pop() : targetChildren },
        enumerable: true },
    oBringParentsChain: // Retorna arranjo com os elementos ascendentes do alvo, do mais próximo ao mais distante
      { value: function oBringParentChain( baseElement = document.documentElement )
          { oAssertConstructor( { Element: baseElement } );
            let depth = this.oFindDepth( baseElement ), parentsList = [];
            for( let actualParent = this.parentElement, i = 0; i < depth; actualParent = actualParent.parentElement, i++ ) parentsList.push( actualParent );
            return parentsList },
        enumerable: true },
    oBringParents: // Retorna um arranjo com os elementos do primeiro argumento ascendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringParents( elementsList = this.oBringParentsChain(), count = Infinity, range = Infinity )
          { oAssertType( { number: [ count, range ] } );
            let actualParent = this.parentElement, targetParents = [];
            if( count <= 0 ) return [];
            if( range < 0 ) if( ( range = this.oFindDepth() + range ) < 0 ) return count == 1 ? false : [];
            let arrayList = new oElementsArray( elementsList );
            parentsLoop: while( actualParent && arrayList.length && range )
              { while( arrayList.includes( actualParent ) )
                  { targetParents = targetParents.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( actualParent ) ), 1 ) );
                    if( targetParents.length == count ) break parentsLoop }
                actualParent = actualParent.parentElement; range-- }
            return count == 1 ? targetParents.pop() : targetParents },
        enumerable: true },
    oDisableEventType: // Desabilita evento se condição retornar valor verdadeiro
      { value: function oDisableEventType( event, parent = this.parentElement.tagName, condition = true )
          { oAssertType( { string: [ event, parent ] } );
            if( !condition ) return false;
            let target = this.parentElement.closest( parent );
            target.addEventListener( event, oBlockEvent, true );
            return true },
        enumerable: true },
    oRenableEventType: // Reabilita evento se condição retornar valor verdadeiro
      { value: function oRenableEventType( event, parent = this.parentElement.tagName, condition = true )
          { oAssertType( { string: [ event, parent ] } );
            if( !condition ) return false;
            let target = this.parentElement.closest( parent );
            target.removeEventListener( event, oBlockEvent, true );
            return true },
        enumerable: true },
    oRestrictClass: // Dentro de dado grupo, restringe uma classe de modo que apenas o alvo a tenha
      { value: function oRestrictClass( _class, elementsGroup = this.parentElement.children, reset = false )
          { oAssertType( { string: _class } );
            let groupList = new oElementsArray( elementsGroup );
            if( !reset && this.classList.contains( _class ) ) return;
            for( let element of elementsGroup ) element.classList.remove( _class );
            this.classList.add( _class ) },
        enumerable: true } } );

/*---- Elementos do HTML ----*/

// Elementos do HTML – Protótipo

Object.defineProperties( HTMLElement.prototype,
  { oAdjustSize: // Altera largura [e/ou] altura do elemento para medidas passadas
      { value: function oAdjustSize( sizeMap, unit = "rem" )
          { if( !sizeMap )
              { let sizeArray = this.oGetActualSize();
                sizeMap[ "width" ] = sizeArray[0]; sizeMap[ "height" ] = sizeArray[1] }
            let allowedKeys = [ "minWidth", "minHeight", "width", "height", "maxWidth", "maxHeight" ];
            for( let size in sizeMap )
              { oAssert( allowedKeys.includes( size ), "Invalid key in argument 'sizeMap' of 'oAdjustSize.", RangeError );
                oAssertType( { number: sizeMap[ size ] } );
                let newSize;
                switch( unit )
                  { case "px": newSize = sizeMap[ size ] + "px"; break;
                    case "rem": newSize = sizeMap[ size ] / 10 + "rem"; break;
                    default: throw new RangeError( "The argument 'unit' of 'oAdjustToActualSize' must be 'px' or 'rem'." ) }
                this.style[ size ] = newSize } },
        enumerable: true },
    oGetActualSize: // Retorna largura [e/ou] altura aparentes do elemento
      { value: function oGetActualSize( sizeMap = { width: true, height: true } )
          { let lastChild = Array.from( this.children ).reverse().find( element => window.getComputedStyle( element ).display != "none" ),
                sizeArray = [];
            if( sizeMap[ "width" ] ) lastChild ? sizeArray.push( lastChild.offsetLeft + lastChild.offsetWidth ) : sizeArray.push( 0 );
            if( sizeMap[ "height" ] ) lastChild ? sizeArray.push( lastChild.offsetTop + lastChild.offsetHeight ) : sizeArray.push( 0 );
            return sizeArray.length > 1 ? sizeArray : sizeArray.pop() },
        enumerable: true },
    oMatchSize: // Redimensiona alvo em relação a dado elemento
      { value: function oMatchSize( element, sizeMap = { width: "offsetWidth", height: "offsetHeight" }, multiplier = 1, unit = "rem" )
          { oAssertConstructor( { HTMLElement: element } );
            oAssertType( { number: multiplier } );
            let allowedValues = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
            for( let size in sizeMap )
              { oAssert( allowedValues.includes( sizeMap[ size ] ), "Invalid value in argument 'sizeMap' of 'oMatchSize'.", RangeError );
                this.oAdjustSize( { size: element[ sizeMap[ size ] ] }, unit );
                this.style[ size ] = this.style[ size ].replace( /[^0-9.+-]/g, "" ) * multiplier + unit } },
        enumerable: true } } );

/*---- Construtores & Pseudoconstrutores ----*/

// Arranjos de Elementos – Construtor

const oElementsArray = function( elements )
  { let baseArray = Array.oMake( elements );
    oAssertConstructor( { Element: elements } );
    oElementsArray.setProperties( baseArray );
    return baseArray }

Object.defineProperties( oElementsArray,
  { setProperties:
      { value: function setProperties( baseArray ) // Inclui os métodos no protótipo de oElementsArray para o arranjo passado
          { let targetMethods = Object.keys( oElementsArray.prototype );
            for( let method of targetMethods ) baseArray[ method ] = oElementsArray.prototype[ method ];
            return baseArray },
        enumerable: true } } )

// Arranjos de Elementos – Protótipo

Object.defineProperties( oElementsArray.prototype,
  { oSegmentByParent: // Cria subarranjos cujos valores têm um ascendente em comum dentre os da lista do argumento
      { value: function oSegmentByParent( parentsList )
          { return this.oSegment( ( element, index, array ) => element.oBringParents( parentsList, 1 ) != array[ index - 1 ].oBringParents( parentsList, 1 ) ) },
        enumerable: true },
    oSelectForEach: // Itera o alvo e retorna um arranjo com elementos que corresponderem à string de seleção do argumento
      { value: function oSelectForEach( selector, target = "children", count = 1, range = Infinity )
          { oAssertType( { string: selector, number: [ count, range ] } );
            oAssert( count > 0 && range > 0, "The arguments 'count' and 'range' must be greater than 0.", RangeError );
            let selection = [], method;
            switch( target )
              { case "children": method = "oBringChildren"; break;
                case "parents": method = "oBringParentsChain"; break;
                default: throw new RangeError( "The allowed values for argument 'target' are 'children' and 'parents'." ) };
            switch( method )
              { case "oBringChildren": this.forEach( element =>
                  { selection = selection.concat( element.oBringChildren( element.querySelectorAll( selector ), count, range ) || [] ) } ); break;
                case "oBringParentsChain": this.forEach( element =>
                  { let parents = element.oBringParentsChain().slice( 0, range ), matchesCount = 0;
                    for( let parent of parents )
                      { if( parent.matches( selector ) )
                          { selection.push( parent ); matchesCount++;
                            if( matchesCount == count ) break } } } ) }
            return selection },
        enumerable: true },
    oSowClass: // Adiciona classe a todos os elementos para os que a função de retorno de chamada retornar valor verdadeiro
      { value: function oSowClass( _class, _function )
          { this.forEach( ( element, index, array ) =>
              { if( _function( element, index, array ) ) element.classList.add( _class ) } ) },
        enumerable: true } } );
