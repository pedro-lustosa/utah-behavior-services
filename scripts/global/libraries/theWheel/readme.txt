theWheel is a library under development by the developer that did this project, and has yet to be documented and internationalized for others ease of use.

Nevertheless, (most of) the functions are stable enough to be used for anyone who care.

The file "_theWhell.js" contains all the library's functions as of the time where this project for the Utah dashboard was finished (12/28/2018); the file "theWheel.js" contains all the functions actually used in the dashboard pages of the project; the file "theWheel-min.js" is "theWheel.js" minified.