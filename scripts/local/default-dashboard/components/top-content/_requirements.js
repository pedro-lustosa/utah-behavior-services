/* Construtores */

import { Menu1 } from "../../../../global/site-presets/referrers/constructors/_menu-1.js";

/* Identificadores */

export const requirements = document.getElementById( "requirements" );

requirements:
  { var menu = requirements.menu = requirements.querySelector( ".menu" );
    menu:
      { var selectList = menu.selectList = menu.querySelector( ".select-list" ),
            menuArrows = menu.arrows = Array.from( menu.getElementsByClassName( "arrow-container" ) );
        selectList:
          { var selectListItems = selectList.items = Array.from( selectList.children ).filter( item => item.tagName == "LI" ) }
        menuArrows:
          { var nextArrow = menuArrows.find( arrow => arrow.querySelector( ".rarr" ) ),
                previousArrow = menuArrows.find( arrow => arrow.querySelector( ".larr" ) ) } } }

/* Instanciações */

Menu1:
  { new Menu1( selectList, selectListItems, menuArrows, nextArrow, previousArrow ) }
