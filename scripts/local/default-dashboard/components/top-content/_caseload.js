/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { Menu1 } from "../../../../global/site-presets/referrers/constructors/_menu-1.js";

import { HeightSlider1 } from "../../../../global/site-presets/referrers/constructors/_height-slider-1.js";

import { BoxesList1 } from "../../../../global/site-presets/referrers/constructors/_boxes-list-1.js";

import { RemoveButton1 } from "../../../../global/site-presets/referrers/constructors/_remove-button-1.js";

/* Identificadores */

export const caseload = document.getElementById( "caseload" );

caseload:
  { var menu = caseload.menu = caseload.querySelector( ".menu" ),
        chartsList = caseload.chartsList = caseload.querySelector( ".charts-list" );
    menu:
      { var selectList = menu.selectList = menu.querySelector( ".select-list" ),
            menuArrows = menu.arrows = Array.from( menu.getElementsByClassName( "arrow-container" ) );
        selectList:
          { var selectListItems = selectList.items = Array.from( selectList.children ).filter( item => item.tagName == "LI" ) }
        menuArrows:
          { var nextArrow = menuArrows.find( arrow => arrow.querySelector( ".rarr" ) ),
                previousArrow = menuArrows.find( arrow => arrow.querySelector( ".larr" ) ) } }
    chartsList:
      { var chartsBoxes = chartsList.boxes = new oElementsArray( chartsList.getElementsByClassName( "chart-box" ) );
        chartsBoxes:
          { var fixedRows = chartsBoxes.fixedRows = new oElementsArray( chartsBoxes.oSelectForEach( ".fixed-row" ) ),
                slideArrows = chartsBoxes.slideArrows = chartsBoxes.oSelectForEach( ".arrow-container" ),
                hiddenFields = chartsBoxes.hiddenFields = new oElementsArray( chartsBoxes.oSelectForEach( ".hidden-fields" ) );
            fixedRows:
              { var fixedRowsHeadings = fixedRows.headings = new oElementsArray( fixedRows.oSelectForEach( ".heading-container" ) );
                fixedRowsHeadings:
                  { var noticesList = fixedRowsHeadings.noticesList = new oElementsArray( fixedRowsHeadings.oSelectForEach( ".notice-list" ) );
                    noticesList:
                      { var noticesListSquares = noticesList.squares = new oElementsArray( noticesList.oSelectForEach( ".square", "children", Infinity ) );
                        noticesListSquares:
                          { var noticesListSquaresSet = noticesListSquares.set = noticesListSquares.oSegmentByParent( noticesList ) } } } }
            hiddenFields:
              { var slidingRows = hiddenFields.slidingRows = new oElementsArray( hiddenFields.oSelectForEach( ".sliding-rows" ) ),
                    userReports = hiddenFields.userReports = new oElementsArray( hiddenFields.oSelectForEach( ".user-reports" ) );
                slidingRows:
                  { var rowsToSlide = slidingRows.rows = slidingRows.oSelectForEach( ".row", "children", Infinity ) }
                userReports:
                  { var boxesList = userReports.boxesList = new oElementsArray( userReports.oSelectForEach( ".boxes-list" ) );
                    boxesList:
                      { var reportBoxesListings = boxesList.items = new oElementsArray( boxesList.oSelectForEach( "li" ) ),
                            reportBoxes = boxesList.reportBoxes = new oElementsArray( boxesList.oSelectForEach( ".report-box", "children", Infinity ) );
                        reportBoxes:
                          { var reportBoxesSet = reportBoxes.set = reportBoxes.oSegmentByParent( boxesList ),
                                removeButtons = reportBoxes.removeButtons = new oElementsArray( reportBoxes.oSelectForEach( ".options-list .close" ) );
                            removeButtons:
                              { var removeButtonsSet = removeButtons.set = removeButtons.oSegmentByParent( boxesList ) } } } } } } } }

/* Inquéritos de Mídia */

const media = { maxWidth: [ 1199 ] }

/* Instanciações */

Menu1:
  { new Menu1( selectList, selectListItems, menuArrows, nextArrow, previousArrow ) }

HeightSlider1:
  { for( let i = 0; i < slidingRows.length; i++ ) new HeightSlider1( hiddenFields[i], chartsBoxes[i], slideArrows[i], { adjacentElement: fixedRows[i], overflow: true } ) }

BoxesList1:
  { let userReportsI = userReports.values(),
        reportBoxesSetI = reportBoxesSet.values(),
        removeButtonsSetI = reportBoxesSet.values();
    for( let i = 0; i < hiddenFields.length; i++ )
      { if( !hiddenFields[i].oBringChildren( reportBoxes, 1 ) ) continue;
        new BoxesList1( userReportsI.next().value, hiddenFields[i], reportBoxesSetI.next().value, removeButtonsSetI.next().value, { squaresList: noticesListSquaresSet[i] } ) } }

RemoveButton1:
  { for( let i = 0; i < removeButtons.length; i++ ) new RemoveButton1( removeButtons[i], reportBoxes[i].closest( "li" ), { fade: true } ) }

/* Eventos */

heightAdjustments:
  { slidingRows:
      { window.addEventListener( "load", configRowsToSlideHeight ) } }

/* Funções */

function configRowsToSlideHeight()
  { setHeight();
    window.addEventListener( "resize", setHeight );
    function setHeight()
      { let sampleFixedRow = fixedRows[0],
            sampleSlidingRow = rowsToSlide[0];
        if( window.innerWidth > media.maxWidth[0] && window.getComputedStyle( sampleSlidingRow ).minHeight.replace( /[^1-9+-.]/g, "" ) != sampleFixedRow.offsetHeight ) return expandHeight();
        if( window.innerWidth <= media.maxWidth[0] && sampleSlidingRow.style.minHeight )
          { for( let row of rowsToSlide ) row.style.minHeight = "" } }
    function expandHeight()
      { for( let row of rowsToSlide )
          { let chartBox = row.oBringParents( chartsBoxes, 1 ),
                fixedRow = chartBox.oBringChildren( fixedRows, 1 );
            row.style.minHeight = fixedRow.offsetHeight / 10 + "rem" } } }
