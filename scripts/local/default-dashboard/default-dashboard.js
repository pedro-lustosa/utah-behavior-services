( function()
    { /* Construtores */

        /*---- Menu 1 ----*/

          // Construtor

          const Menu1 = function( self, items, arrows, nextArrow, previousArrow )
            { assignReferrers:
                { this.self = self; this.items = items, this.arrows = arrows;
                  arrows:
                    { arrows.next = nextArrow; arrows.previous = previousArrow } }
              assignEvents:
                { for( let item of items ) item.addEventListener( "click", event => this.changeActiveMenuItem( event ) );
                  for( let arrow of [ nextArrow, previousArrow ] ) arrow.addEventListener( "click", () => this.changeDisplayedMenuItem( arrow, arrow == nextArrow ? +1 : -1 ) );
                  window.addEventListener( "resize", event => this.updateArrowsStyle( event ) ) }
              initialInstructions:
                { this.updateArrowsStyle() } }

          // Inquéritos de Mídia

          const Menu1Media = { maxWidth: [ 599 ] };

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = Menu1.prototype;
              changeMenuStyle:
                { prototype.changeActiveMenuItem = function ( event )
                    { let item = event.currentTarget;
                      item.oRestrictClass( "current", this.items ) }
                  prototype.updateArrowsStyle = function ( event )
                    { if( window.innerWidth > Menu1Media.maxWidth[ 0 ] && event && event.type == "resize" ) return;
                      let items = this.items,
                          currentMenuItem = items.find( item => item.classList.contains( "current" ) ),
                          previousArrow = this.arrows.previous,
                          nextArrow = this.arrows.next;
                      nextArrow.classList.toggle( "clickable", items[ items.indexOf( currentMenuItem ) + 1 ] != undefined );
                      previousArrow.classList.toggle( "clickable", items[ items.indexOf( currentMenuItem ) - 1 ] != undefined ) } }
              changeDisplayedMenuItem:
                { prototype.changeDisplayedMenuItem = function ( arrow, direction )
                    { let oppositeArrow = this.arrows.find( _arrow => _arrow != arrow ),
                          items = this.items,
                          currentIndex = items.findIndex( item => item.classList.contains( "current" ) ),
                          newItem;
                      if( ( newItem = items[ currentIndex + direction ] ) == undefined ) return;
                      newItem.oRestrictClass( "current", items );
                      this.updateArrowsStyle() } } }

        /*---- Height Slider 1 ----*/

          // Construtor

          const HeightSlider1 = function( slidedElement, referenceElement, trigger,
                                                 featuresMap = { overflow: false, adjacentElement: false, expandSlidedParent: false, shrinkSlidedChild: false } )
            { assignReferrers:
                { this.slidedElement = slidedElement; this.referenceElement = referenceElement; this.trigger = trigger; this.featuresMap = featuresMap }
              assignCoreEvents:
                { let slide = this.slide.bind( this, slidedElement, referenceElement, trigger ),
                      renableClicksInTrigger = this.renableClicksInTrigger.bind( this, trigger ),
                      readjustSlidedElementHeight = this.readjustSlidedElementHeight.bind( this, slidedElement, referenceElement );
                  trigger.addEventListener( "click", slide );
                  trigger.addEventListener( "transitionend", renableClicksInTrigger );
                  window.addEventListener( "resize", readjustSlidedElementHeight ) }
              featuresMapAddOns:
                { expandSlidedParent:
                    { if( !featuresMap.expandSlidedParent ) break expandSlidedParent;
                      var slidedParent = featuresMap.expandSlidedParent;
                      let updateParentHeight = this.updateParentHeight.bind( this, slidedElement, referenceElement, slidedParent );
                      trigger.addEventListener( "click", updateParentHeight ) }
                  shrinkSlidedChild:
                    { if( !featuresMap.shrinkSlidedChild ) break shrinkSlidedChild;
                      var slidedChild = featuresMap.shrinkSlidedChild.slidedChild,
                          slidedChildReferenceElement = featuresMap.shrinkSlidedChild.referenceElement;
                      let updateChildHeight = this.updateChildHeight.bind( this, slidedChild, slidedChildReferenceElement );
                      trigger.addEventListener( "click", updateChildHeight ) }
                  adjacentElement:
                    { if( featuresMap.adjacentElement ) var adjacentElement = this.adjacentElement = featuresMap.adjacentElement }
                  overflow:
                    { if( !featuresMap.overflow ) break overflow;
                      let triggerHiddenOverflow = this.triggerHiddenOverflow.bind( this, slidedElement, adjacentElement ),
                          triggerVisibleOverflow = this.triggerVisibleOverflow.bind( this, slidedElement, referenceElement, adjacentElement );
                      trigger.addEventListener( "click", triggerHiddenOverflow );
                      slidedElement.addEventListener( "transitionend", triggerVisibleOverflow ) } } }

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = HeightSlider1.prototype;
              slideTargetElement:
                { prototype.slide = function( slidedElement, referenceElement, trigger, event )
                    { if( trigger.closest( ".disabled" ) ) return;
                      trigger.oDisableEventType( "click" );
                      slidedElement.style.height = referenceElement.classList.toggle( "extended" ) ? slidedElement.scrollHeight / 10 + "rem" : "0" }
                  prototype.readjustSlidedElementHeight = function( slidedElement, referenceElement, event )
                    { if( !referenceElement.classList.contains( "extended" ) ) return;
                      let actualHeight = slidedElement.oGetActualSize( { height: true } );
                      if( slidedElement.offsetHeight != actualHeight ) slidedElement.oAdjustSize( { height: actualHeight } ) }
                  prototype.renableClicksInTrigger = function( trigger, event )
                    { if( !trigger.closest( ".disabled" ) ) trigger.oRenableEventType( "click" ) } }
              overflowAdjustments:
                { prototype.triggerHiddenOverflow = function( slidedElement, adjacentElement, event )
                    { let targetElements = [].concat( slidedElement, adjacentElement || [] );
                      for( let element of targetElements ) element.style.overflow = "hidden" }
                  prototype.triggerVisibleOverflow = function( slidedElement, referenceElement, adjacentElement, event )
                    { if( adjacentElement ) adjacentElement.style.overflow = "visible";
                      if( referenceElement.classList.contains( "extended" ) ) slidedElement.style.overflow = "visible" } }
              adjustSlidedParentHeight:
                { prototype.updateParentHeight = function( slidedElement, referenceElement, slidedParent, event )
                    { slidedParent.style.height = ( referenceElement.classList.contains( "extended" ) ?
                        slidedParent.offsetHeight + slidedElement.scrollHeight : slidedParent.offsetHeight - slidedElement.scrollHeight ) / 10 + "rem" } }
              adjustSlidedChildHeight:
                { prototype.updateChildHeight = function( slidedChild, referenceElement, event )
                    { if( !referenceElement.classList.contains( "extended" ) ) return;
                      slidedChild.style.height = "0"; referenceElement.classList.remove( "extended" ) } } }

        /*---- Boxes List 1 ----*/

          // Construtor

          const BoxesList1 = function( boxesSection, sectionContainer, boxes, removeButtons, featuresMap = { squaresList: false } )
            { assignReferrers:
                { this.boxesSection = boxesSection; this.sectionContainer = sectionContainer; this.boxes = boxes; this.removeButtons = removeButtons; this.featuresMap = featuresMap;
                  featuresMap:
                    { var squaresList = featuresMap.squaresList;
                      squaresList:
                        { if( !squaresList ) break squaresList;
                          var activeSquares = squaresList.filter( square => square.classList.contains( "new" ) ) } } }
              assignEvents:
                { removeButtons:
                    { for( let i = 0; i < removeButtons.length; i++ )
                        { let handlersArray = [];
                          coreEvents:
                            { let adjustDistance = this.adjustDistance.bind( this, boxesSection, sectionContainer, boxes, boxes[i] );
                              handlersArray.push( adjustDistance ) }
                          squaresList:
                            { if( !squaresList ) break squaresList;
                              let defaultSquare = this.defaultSquare.bind( this, activeSquares[i] );
                              handlersArray.push( defaultSquare ) }
                          for( let handler of handlersArray ) removeButtons[i].addEventListener( "click", handler ) } } } }

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = BoxesList1.prototype;
              distanceAdjustments:
                { prototype.adjustDistance = function( boxesSection, sectionContainer, boxes, targetBox, event )
                    { if( boxes.some( box => !box.isSameNode( targetBox ) && boxesSection.contains( box ) ) ) return;
                      targetBox.closest( "li" ).addEventListener( "transitionend", revalueBoxesSectionHeight );
                      function revalueBoxesSectionHeight( event )
                        { if( event.target != event.currentTarget || event.propertyName != "opacity" ) return;
                          boxesSection.style.display = "none";
                          sectionContainer.oAdjustSize( { height: sectionContainer.oGetActualSize( { height: true } ) } );
                          this.removeEventListener( "transitionend", revalueBoxesSectionHeight ) } } }
              squaresList:
                { prototype.defaultSquare = function( targetSquare, event )
                    { targetSquare.classList.remove( "new" ); targetSquare.style.backgroundColor = "transparent" } } }

        /*---- Remove Button 1 ----*/

          // Construtor

          const RemoveButton1 = function( button, removedContent, featuresMap = { softRemoval: false, fade: false } )
            { assignReferrers:
                { this.button = button; this.removedContent = removedContent; this.featuresMap = featuresMap }
              assignEvents:
                { let removeContent = this.removeContent.bind( this, removedContent, featuresMap.softRemoval ),
                      fadeContent = this.fadeContent.bind( this, removedContent );
                  if( featuresMap.fade )
                    { button.addEventListener( "click", fadeContent );
                      removedContent.addEventListener( "transitionend", removeContent ) }
                  else
                    { button.addEventListener( "click", removeContent ) } } }

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = RemoveButton1.prototype;
              contentRemoval:
                { prototype.fadeContent = function ( removedContent, event )
                    { removedContent.style.opacity = "0" }
                  prototype.removeContent = function ( removedContent, softRemoval = false, event )
                    { if( event.type == "transitionend" && ( event.target != event.currentTarget || event.propertyName != "opacity" || window.getComputedStyle( event.target ).opacity != "0" ) ) return;
                      softRemoval ? removedContent.style.display = "none" : removedContent.remove() } } }

      /* Componentes */

        // Caseload

        ( function()
            { /* Identificadores */

              const caseload = document.getElementById( "caseload" );

              caseload:
                { var menu = caseload.menu = caseload.querySelector( ".menu" ),
                      chartsList = caseload.chartsList = caseload.querySelector( ".charts-list" );
                  menu:
                    { var selectList = menu.selectList = menu.querySelector( ".select-list" ),
                          menuArrows = menu.arrows = Array.from( menu.getElementsByClassName( "arrow-container" ) );
                      selectList:
                        { var selectListItems = selectList.items = Array.from( selectList.children ).filter( item => item.tagName == "LI" ) }
                      menuArrows:
                        { var nextArrow = menuArrows.find( arrow => arrow.querySelector( ".rarr" ) ),
                              previousArrow = menuArrows.find( arrow => arrow.querySelector( ".larr" ) ) } }
                  chartsList:
                    { var chartsBoxes = chartsList.boxes = new oElementsArray( chartsList.getElementsByClassName( "chart-box" ) );
                      chartsBoxes:
                        { var fixedRows = chartsBoxes.fixedRows = new oElementsArray( chartsBoxes.oSelectForEach( ".fixed-row" ) ),
                              slideArrows = chartsBoxes.slideArrows = chartsBoxes.oSelectForEach( ".arrow-container" ),
                              hiddenFields = chartsBoxes.hiddenFields = new oElementsArray( chartsBoxes.oSelectForEach( ".hidden-fields" ) );
                          fixedRows:
                            { var fixedRowsHeadings = fixedRows.headings = new oElementsArray( fixedRows.oSelectForEach( ".heading-container" ) );
                              fixedRowsHeadings:
                                { var noticesList = fixedRowsHeadings.noticesList = new oElementsArray( fixedRowsHeadings.oSelectForEach( ".notice-list" ) );
                                  noticesList:
                                    { var noticesListSquares = noticesList.squares = new oElementsArray( noticesList.oSelectForEach( ".square", "children", Infinity ) );
                                      noticesListSquares:
                                        { var noticesListSquaresSet = noticesListSquares.set = noticesListSquares.oSegmentByParent( noticesList ) } } } }
                          hiddenFields:
                            { var slidingRows = hiddenFields.slidingRows = new oElementsArray( hiddenFields.oSelectForEach( ".sliding-rows" ) ),
                                  userReports = hiddenFields.userReports = new oElementsArray( hiddenFields.oSelectForEach( ".user-reports" ) );
                              slidingRows:
                                { var rowsToSlide = slidingRows.rows = slidingRows.oSelectForEach( ".row", "children", Infinity ) }
                              userReports:
                                { var boxesList = userReports.boxesList = new oElementsArray( userReports.oSelectForEach( ".boxes-list" ) );
                                  boxesList:
                                    { var reportBoxesListings = boxesList.items = new oElementsArray( boxesList.oSelectForEach( "li" ) ),
                                          reportBoxes = boxesList.reportBoxes = new oElementsArray( boxesList.oSelectForEach( ".report-box", "children", Infinity ) );
                                      reportBoxes:
                                        { var reportBoxesSet = reportBoxes.set = reportBoxes.oSegmentByParent( boxesList ),
                                              removeButtons = reportBoxes.removeButtons = new oElementsArray( reportBoxes.oSelectForEach( ".options-list .close" ) );
                                          removeButtons:
                                            { var removeButtonsSet = removeButtons.set = removeButtons.oSegmentByParent( boxesList ) } } } } } } } }

              /* Inquéritos de Mídia */

              const media = { maxWidth: [ 1199 ] }

              /* Instanciações */

              Menu1:
                { new Menu1( selectList, selectListItems, menuArrows, nextArrow, previousArrow ) }

              HeightSlider1:
                { for( let i = 0; i < slidingRows.length; i++ ) new HeightSlider1( hiddenFields[i], chartsBoxes[i], slideArrows[i], { adjacentElement: fixedRows[i], overflow: true } ) }

              BoxesList1:
                { let userReportsI = userReports.values(),
                      reportBoxesSetI = reportBoxesSet.values(),
                      removeButtonsSetI = reportBoxesSet.values();
                  for( let i = 0; i < hiddenFields.length; i++ )
                    { if( !hiddenFields[i].oBringChildren( reportBoxes, 1 ) ) continue;
                      new BoxesList1( userReportsI.next().value, hiddenFields[i], reportBoxesSetI.next().value, removeButtonsSetI.next().value, { squaresList: noticesListSquaresSet[i] } ) } }

              RemoveButton1:
                { for( let i = 0; i < removeButtons.length; i++ ) new RemoveButton1( removeButtons[i], reportBoxes[i].closest( "li" ), { fade: true } ) }

              /* Eventos */

              heightAdjustments:
                { slidingRows:
                    { window.addEventListener( "load", configRowsToSlideHeight ) } }

              /* Funções */

              function configRowsToSlideHeight()
                { setHeight();
                  window.addEventListener( "resize", setHeight );
                  function setHeight()
                    { let sampleFixedRow = fixedRows[0],
                          sampleSlidingRow = rowsToSlide[0];
                      if( window.innerWidth > media.maxWidth[0] && window.getComputedStyle( sampleSlidingRow ).minHeight.replace( /[^1-9+-.]/g, "" ) != sampleFixedRow.offsetHeight ) return expandHeight();
                      if( window.innerWidth <= media.maxWidth[0] && sampleSlidingRow.style.minHeight )
                        { for( let row of rowsToSlide ) row.style.minHeight = "" } }
                  function expandHeight()
                    { for( let row of rowsToSlide )
                        { let chartBox = row.oBringParents( chartsBoxes, 1 ),
                              fixedRow = chartBox.oBringChildren( fixedRows, 1 );
                          row.style.minHeight = fixedRow.offsetHeight / 10 + "rem" } } } } )();

        // Requirements

        ( function()
            { /* Identificadores */

              const requirements = document.getElementById( "requirements" );

              requirements:
                { var menu = requirements.menu = requirements.querySelector( ".menu" );
                  menu:
                    { var selectList = menu.selectList = menu.querySelector( ".select-list" ),
                          menuArrows = menu.arrows = Array.from( menu.getElementsByClassName( "arrow-container" ) );
                      selectList:
                        { var selectListItems = selectList.items = Array.from( selectList.children ).filter( item => item.tagName == "LI" ) }
                      menuArrows:
                        { var nextArrow = menuArrows.find( arrow => arrow.querySelector( ".rarr" ) ),
                              previousArrow = menuArrows.find( arrow => arrow.querySelector( ".larr" ) ) } } }

              /* Instanciações */

              Menu1:
                { new Menu1( selectList, selectListItems, menuArrows, nextArrow, previousArrow ) } } )() } )();
