( function()
    { /*---- Construtores ----*/

        /*---- Table 1 ----*/

        // Construtor

        const Table1 = function( table, rows, allCells, cellsSet )
          { assignReferrers:
              { this.table = table; this.rows = rows; this.cells = { all: allCells, set: cellsSet } }
            assignEvents:
              {  }
            initialInstructions:
              { this.hiddenFilledBorders( cellsSet ) } }

        // Chaves do Protótipo

        prototypeKeys:
          { let prototype = Table1.prototype;
            filledCells:
              { prototype.hiddenFilledBorders = function( cellsSet )
                  { cellsSet.forEach( ( set, index, array ) =>
                      { for( let i = 0; i < set.length; i++ )
                          { let cell = set[i],
                                nextSet = array[ index + 1 ];
                            if( !nextSet ) continue;
                            if( !cell.classList.contains( "filled" ) ) { cell.style.borderBottomColor = ""; continue };
                            let nextCell = nextSet[i];
                            if( nextCell.classList.contains( "filled" ) && !nextCell.innerText ) cell.style.borderBottomColor = "transparent" } } ) } } }

        /*---- Modal Box 1 ----*/

        // Construtor

        const ModalBox1 = function( modalContainer, modal, triggers, exitButton, featuresMap = { hideBody: false } )
          { assignReferrers:
              { this.modalContainer = modalContainer; this.modal = modal; this.triggers = triggers; this.exitButton = exitButton; this.featuresMap = featuresMap;
                featuresMap:
                  { var hideBody = featuresMap.hideBody } }
            assignConstructors:
              { new RemoveButton1( exitButton, modal, { softRemoval: true, fade: true } ) }
            assignEvents:
              { let revealModalContainer = this.revealModalContainer.bind( this, modalContainer, hideBody ),
                    revealModal = this.revealModal.bind( this, modalContainer, modal ),
                    hiddenModalContainer = this.hiddenModalContainer.bind( this, modalContainer, modal ),
                    resetModalContainer = this.resetModalContainer.bind( this, modalContainer, modal, hideBody ),
                    revealModalContainerOnTouchTime = this.revealModalContainerOnTouchTime.bind( this, modalContainer, hideBody );
                for( let trigger of triggers )
                  { trigger.addEventListener( "touchstart", revealModalContainerOnTouchTime );
                    trigger.addEventListener( "dblclick", revealModalContainer ) };
                for( let _event of [ revealModal, resetModalContainer ] ) modalContainer.addEventListener( "transitionend", _event );
                modal.addEventListener( "transitionend", hiddenModalContainer ) } }

        // Chaves do Protótipo

        prototypeKeys:
          { let prototype = ModalBox1.prototype;
            transitionControl:
              { prototype.revealModalContainerOnTouchTime = function( modalContainer, hideBody, event )
                  { let triggerTime = 600,
                        revealModalContainer = window.setTimeout( prototype.revealModalContainer, triggerTime, modalContainer, hideBody );
                    event.target.addEventListener( "touchend", () => clearTimeout( revealModalContainer ), { once: true } );
                    event.preventDefault() }
                prototype.revealModalContainer = function( modalContainer, hideBody, event )
                  { modalContainer.style.visibility = "visible"; modalContainer.style.opacity = "1";
                    if( hideBody ) document.body.style.overflow = "hidden" }
                prototype.revealModal = function( modalContainer, modal, event )
                  { if( modalContainer != event.target || event.propertyName != "opacity" || window.getComputedStyle( modalContainer ).opacity != "1" ) return;
                    modal.style.visibility = "visible"; modal.style.opacity = "1" }
                prototype.hiddenModalContainer = function( modalContainer, modal, event )
                  { if( modal != event.target || event.propertyName != "opacity" || window.getComputedStyle( modal ).opacity != "0" ) return;
                    modalContainer.style.opacity = "0" }
                prototype.resetModalContainer = function( modalContainer, modal, hideBody, event )
                  { if( modalContainer != event.target || event.propertyName != "opacity" || window.getComputedStyle( modalContainer ).opacity != "0" ) return;
                    modalContainer.style.visibility = "hidden";
                    modal.style.visibility = "hidden"; modal.style.display = "";
                    if( hideBody ) document.body.style.overflow = "" } } }

        /*---- Remove Button 1 ----*/

        // Construtor

        const RemoveButton1 = function( button, removedContent, featuresMap = { softRemoval: false, fade: false } )
          { assignReferrers:
              { this.button = button; this.removedContent = removedContent; this.featuresMap = featuresMap }
            assignEvents:
              { let removeContent = this.removeContent.bind( this, removedContent, featuresMap.softRemoval ),
                    fadeContent = this.fadeContent.bind( this, removedContent );
                if( featuresMap.fade )
                  { button.addEventListener( "click", fadeContent );
                    removedContent.addEventListener( "transitionend", removeContent ) }
                else
                  { button.addEventListener( "click", removeContent ) } } }

        // Chaves do Protótipo

        prototypeKeys:
          { let prototype = RemoveButton1.prototype;
            contentRemoval:
              { prototype.fadeContent = function ( removedContent, event )
                  { removedContent.style.opacity = "0" }
                prototype.removeContent = function ( removedContent, softRemoval = false, event )
                  { if( event.type == "transitionend" && ( event.target != event.currentTarget || event.propertyName != "opacity" || window.getComputedStyle( event.target ).opacity != "0" ) ) return;
                    softRemoval ? removedContent.style.display = "none" : removedContent.remove() } } }

      /*---- Componentes ----*/

        // Schedule Management & Config Event Modal

        ( function()
            { // Schedule Management

              /* Identificadores */

              const scheduleManagement = document.getElementById( "schedule-management" );

              scheduleManagement:
                { var scheduleTable = scheduleManagement.table = scheduleManagement.querySelector( ".schedule-table" );
                  scheduleTable:
                    { var tableHead = scheduleTable.head = scheduleTable.tHead,
                          tableBody = scheduleTable.body = scheduleTable.tBodies[0],
                          tableRows = Array.from( scheduleTable.rows );
                      tableBody:
                        { var tableRows = tableBody.tr = new oElementsArray( tableBody.rows );
                          tableRows:
                            { var tableCells = tableRows.cells = new oElementsArray( tableRows.oSelectForEach( "td", "children", Infinity, 1 ) );
                              tableCells:
                                { var tableCellsGroup = tableCells.group = tableCells.oSegmentByParent( tableRows ) } } } } }

              /* Instanciações */

              Table1:
                { new Table1( scheduleTable, tableRows, tableCells, tableCellsGroup ) }

              // Config Event Modal

              ( function()
                  { /* Identificadores */

                    const configEventModal = document.getElementById( "config-event-modal" );

                    configEventModal:
                      { var modalBox = configEventModal.box = configEventModal.querySelector( ".modal-box" );
                        modalBox:
                          { var eventForm = modalBox.eventForm = modalBox.querySelector( "[ name='config-event-form' ]" ),
                                exitButton = modalBox.exitButton = modalBox.querySelector( "[ name$='-exit' ]" ) } }

                    /* Instanciações */

                    ModalBox1:
                      { new ModalBox1( configEventModal, modalBox, scheduleManagement.table.body.tr.cells, exitButton, { hideBody: true } ) } } )() } )() } )()
