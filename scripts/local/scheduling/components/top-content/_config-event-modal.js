/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { ModalBox1 } from "../../../../global/site-presets/referrers/constructors/_modal-box-1.js";

/* Variáveis Externas */

import { scheduleManagement } from "./_schedule-management.js";

/* Identificadores */

export const configEventModal = document.getElementById( "config-event-modal" );

configEventModal:
  { var modalBox = configEventModal.box = configEventModal.querySelector( ".modal-box" );
    modalBox:
      { var eventForm = modalBox.eventForm = modalBox.querySelector( "[ name='config-event-form' ]" ),
            exitButton = modalBox.exitButton = modalBox.querySelector( "[ name$='-exit' ]" ) } }

/* Instanciações */

ModalBox1:
  { new ModalBox1( configEventModal, modalBox, scheduleManagement.table.body.tr.cells, exitButton, { hideBody: true } ) }
