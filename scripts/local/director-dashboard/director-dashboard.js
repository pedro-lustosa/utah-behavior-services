( function()
    { /* Construtores */

        /*---- Height Slider 1 ----*/

          // Construtor

          const HeightSlider1 = function( slidedElement, referenceElement, trigger,
                                                 featuresMap = { overflow: false, adjacentElement: false, expandSlidedParent: false, shrinkSlidedChild: false } )
            { assignReferrers:
                { this.slidedElement = slidedElement; this.referenceElement = referenceElement; this.trigger = trigger; this.featuresMap = featuresMap }
              assignCoreEvents:
                { let slide = this.slide.bind( this, slidedElement, referenceElement, trigger ),
                      renableClicksInTrigger = this.renableClicksInTrigger.bind( this, trigger ),
                      readjustSlidedElementHeight = this.readjustSlidedElementHeight.bind( this, slidedElement, referenceElement );
                  trigger.addEventListener( "click", slide );
                  trigger.addEventListener( "transitionend", renableClicksInTrigger );
                  window.addEventListener( "resize", readjustSlidedElementHeight ) }
              featuresMapAddOns:
                { expandSlidedParent:
                    { if( !featuresMap.expandSlidedParent ) break expandSlidedParent;
                      var slidedParent = featuresMap.expandSlidedParent;
                      let updateParentHeight = this.updateParentHeight.bind( this, slidedElement, referenceElement, slidedParent );
                      trigger.addEventListener( "click", updateParentHeight ) }
                  shrinkSlidedChild:
                    { if( !featuresMap.shrinkSlidedChild ) break shrinkSlidedChild;
                      var slidedChild = featuresMap.shrinkSlidedChild.slidedChild,
                          slidedChildReferenceElement = featuresMap.shrinkSlidedChild.referenceElement;
                      let updateChildHeight = this.updateChildHeight.bind( this, slidedChild, slidedChildReferenceElement );
                      trigger.addEventListener( "click", updateChildHeight ) }
                  adjacentElement:
                    { if( featuresMap.adjacentElement ) var adjacentElement = this.adjacentElement = featuresMap.adjacentElement }
                  overflow:
                    { if( !featuresMap.overflow ) break overflow;
                      let triggerHiddenOverflow = this.triggerHiddenOverflow.bind( this, slidedElement, adjacentElement ),
                          triggerVisibleOverflow = this.triggerVisibleOverflow.bind( this, slidedElement, referenceElement, adjacentElement );
                      trigger.addEventListener( "click", triggerHiddenOverflow );
                      slidedElement.addEventListener( "transitionend", triggerVisibleOverflow ) } } }

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = HeightSlider1.prototype;
              slideTargetElement:
                { prototype.slide = function( slidedElement, referenceElement, trigger, event )
                    { if( trigger.closest( ".disabled" ) ) return;
                      trigger.oDisableEventType( "click" );
                      slidedElement.style.height = referenceElement.classList.toggle( "extended" ) ? slidedElement.scrollHeight / 10 + "rem" : "0" }
                  prototype.readjustSlidedElementHeight = function( slidedElement, referenceElement, event )
                    { if( !referenceElement.classList.contains( "extended" ) ) return;
                      let actualHeight = slidedElement.oGetActualSize( { height: true } );
                      if( slidedElement.offsetHeight != actualHeight ) slidedElement.oAdjustSize( { height: actualHeight } ) }
                  prototype.renableClicksInTrigger = function( trigger, event )
                    { if( !trigger.closest( ".disabled" ) ) trigger.oRenableEventType( "click" ) } }
              overflowAdjustments:
                { prototype.triggerHiddenOverflow = function( slidedElement, adjacentElement, event )
                    { let targetElements = [].concat( slidedElement, adjacentElement || [] );
                      for( let element of targetElements ) element.style.overflow = "hidden" }
                  prototype.triggerVisibleOverflow = function( slidedElement, referenceElement, adjacentElement, event )
                    { if( adjacentElement ) adjacentElement.style.overflow = "visible";
                      if( referenceElement.classList.contains( "extended" ) ) slidedElement.style.overflow = "visible" } }
              adjustSlidedParentHeight:
                { prototype.updateParentHeight = function( slidedElement, referenceElement, slidedParent, event )
                    { slidedParent.style.height = ( referenceElement.classList.contains( "extended" ) ?
                        slidedParent.offsetHeight + slidedElement.scrollHeight : slidedParent.offsetHeight - slidedElement.scrollHeight ) / 10 + "rem" } }
              adjustSlidedChildHeight:
                { prototype.updateChildHeight = function( slidedChild, referenceElement, event )
                    { if( !referenceElement.classList.contains( "extended" ) ) return;
                      slidedChild.style.height = "0"; referenceElement.classList.remove( "extended" ) } } }

        /*---- Boxes List 1 ----*/

          // Construtor

          const BoxesList1 = function( boxesSection, sectionContainer, boxes, removeButtons, featuresMap = { squaresList: false } )
            { assignReferrers:
                { this.boxesSection = boxesSection; this.sectionContainer = sectionContainer; this.boxes = boxes; this.removeButtons = removeButtons; this.featuresMap = featuresMap;
                  featuresMap:
                    { var squaresList = featuresMap.squaresList;
                      squaresList:
                        { if( !squaresList ) break squaresList;
                          var activeSquares = squaresList.filter( square => square.classList.contains( "new" ) ) } } }
              assignEvents:
                { removeButtons:
                    { for( let i = 0; i < removeButtons.length; i++ )
                        { let handlersArray = [];
                          coreEvents:
                            { let adjustDistance = this.adjustDistance.bind( this, boxesSection, sectionContainer, boxes, boxes[i] );
                              handlersArray.push( adjustDistance ) }
                          squaresList:
                            { if( !squaresList ) break squaresList;
                              let defaultSquare = this.defaultSquare.bind( this, activeSquares[i] );
                              handlersArray.push( defaultSquare ) }
                          for( let handler of handlersArray ) removeButtons[i].addEventListener( "click", handler ) } } } }

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = BoxesList1.prototype;
              distanceAdjustments:
                { prototype.adjustDistance = function( boxesSection, sectionContainer, boxes, targetBox, event )
                    { if( boxes.some( box => !box.isSameNode( targetBox ) && boxesSection.contains( box ) ) ) return;
                      targetBox.closest( "li" ).addEventListener( "transitionend", revalueBoxesSectionHeight );
                      function revalueBoxesSectionHeight( event )
                        { if( event.target != event.currentTarget || event.propertyName != "opacity" ) return;
                          boxesSection.style.display = "none";
                          sectionContainer.oAdjustSize( { height: sectionContainer.oGetActualSize( { height: true } ) } );
                          this.removeEventListener( "transitionend", revalueBoxesSectionHeight ) } } }
              squaresList:
                { prototype.defaultSquare = function( targetSquare, event )
                    { targetSquare.classList.remove( "new" ); targetSquare.style.backgroundColor = "transparent" } } }

        /*---- Remove Button 1 ----*/

          // Construtor

          const RemoveButton1 = function( button, removedContent, featuresMap = { softRemoval: false, fade: false } )
            { assignReferrers:
                { this.button = button; this.removedContent = removedContent; this.featuresMap = featuresMap }
              assignEvents:
                { let removeContent = this.removeContent.bind( this, removedContent, featuresMap.softRemoval ),
                      fadeContent = this.fadeContent.bind( this, removedContent );
                  if( featuresMap.fade )
                    { button.addEventListener( "click", fadeContent );
                      removedContent.addEventListener( "transitionend", removeContent ) }
                  else
                    { button.addEventListener( "click", removeContent ) } } }

          // Chaves do Protótipo

          prototypeKeys:
            { let prototype = RemoveButton1.prototype;
              contentRemoval:
                { prototype.fadeContent = function ( removedContent, event )
                    { removedContent.style.opacity = "0" }
                  prototype.removeContent = function ( removedContent, softRemoval = false, event )
                    { if( event.type == "transitionend" && ( event.target != event.currentTarget || event.propertyName != "opacity" || window.getComputedStyle( event.target ).opacity != "0" ) ) return;
                      softRemoval ? removedContent.style.display = "none" : removedContent.remove() } } }

      /* Componentes */

        // Users Performance

        ( function()
            { /* Identificadores */

              const usersPerformance = document.getElementById( "users-performance" );

              usersPerformance:
                { var usersList = usersPerformance.usersList = usersPerformance.querySelector( ".users-list" );
                  usersList:
                    { var boxesContainers = usersList.boxesContainers = new oElementsArray( usersList.getElementsByClassName( "box-container" ) );
                      boxesContainers:
                        { var contentHeadings = boxesContainers.headings = new oElementsArray( boxesContainers.oSelectForEach( ".heading-container" ) ),
                              contentBodies = boxesContainers.bodies = new oElementsArray( boxesContainers.oSelectForEach( ".content-body" ) );
                          contentHeadings:
                            { var headingsArrows = contentHeadings.oSelectForEach( ".arrow-container" ) }
                          contentBodies:
                            { var contentFooters = contentBodies.footers = new oElementsArray( contentBodies.oSelectForEach( ".additional-info" ) );
                              contentFooters:
                                { var expandOptions = contentFooters.expandOptions = new oElementsArray( contentFooters.oSelectForEach( ".expand-option" ) ),
                                      hiddenFields = contentFooters.hiddenFields = new oElementsArray( contentFooters.oSelectForEach( ".hidden-fields" ) );
                                  expandOptions:
                                    { var footersArrows = expandOptions.arrows = expandOptions.oSelectForEach( ".arrow-container" ) }
                                  hiddenFields:
                                    { var userReports = hiddenFields.userReports = new oElementsArray( hiddenFields.oSelectForEach( ".user-reports" ) );
                                      userReports:
                                        { var boxesList = userReports.boxesList = new oElementsArray( userReports.oSelectForEach( ".boxes-list" ) );
                                          boxesList:
                                            { var reportBoxesListings = boxesList.items = new oElementsArray( boxesList.oSelectForEach( "li" ) ),
                                                  reportBoxes = boxesList.reportBoxes = new oElementsArray( boxesList.oSelectForEach( ".report-box", "children", Infinity ) );
                                              reportBoxes:
                                                { var reportBoxesSet = reportBoxes.set = reportBoxes.oSegmentByParent( boxesList ),
                                                      removeButtons = reportBoxes.removeButtons = new oElementsArray( reportBoxes.oSelectForEach( ".options-list .close" ) );
                                                  removeButtons:
                                                    { var removeButtonsSet = removeButtons.set = removeButtons.oSegmentByParent( boxesList ) } } } } } } } } } }

              /* Instanciações */

              HeightSlider1:
                { for( let i = 0; i < boxesContainers.length; i++ )
                    { new HeightSlider1( contentBodies[i], boxesContainers[i], headingsArrows[i], { shrinkSlidedChild: { slidedChild: hiddenFields[i], referenceElement: contentBodies[i] } } );
                      new HeightSlider1( hiddenFields[i], contentBodies[i], footersArrows[i], { expandSlidedParent: contentBodies[i] } ) } }

              BoxesList1:
                { let userReportsI = userReports.values(),
                      reportBoxesSetI = reportBoxesSet.values(),
                      removeButtonsSetI = removeButtonsSet.values();
                  for( let i = 0; i < hiddenFields.length; i++ )
                    { if( !hiddenFields[i].oBringChildren( userReports, 1 ) ) continue;
                      new BoxesList1( userReportsI.next().value, hiddenFields[i], reportBoxesSetI.next().value, removeButtonsSetI.next().value ) } }

              RemoveButton1:
                { for( let i = 0; i < removeButtons.length; i++ ) new RemoveButton1( removeButtons[i], reportBoxes[i].closest( "li" ), { fade: true } ) }

              /* Eventos */

              shrinkContentBodiesHeight:
                { for( let list of boxesList )
                    { list.addEventListener( "transitionend", shrinkContentBodyHeight ) } }

              /* Funções */

              function shrinkContentBodyHeight( event )
                { let currentList = event.currentTarget;
                  if( event.target.tagName != "LI" || event.propertyName != "opacity" || currentList.children.length ) return;
                  let contentBody = currentList.oBringParents( contentBodies, 1 ),
                      expandOption = contentBody.oBringChildren( expandOptions, 1 ),
                      hiddenField = contentBody.oBringChildren( hiddenFields, 1 );
                  contentBody.style.height = ( contentBody.offsetHeight - hiddenField.offsetHeight ) / 10 + "rem";
                  contentBody.classList.remove( "extended" );
                  disableExpandOption( expandOption ) }

              function disableExpandOption( expandOption )
                { expandOption.classList.add( "disabled" );
                  expandOption.oBringChildren( footersArrows, 1 ).oDisableEventType( "click" ) }

              /* Instruções Iniciais */

              expandOptions.oSowClass( "disabled", element => !element.oBringParents( contentBodies, 1 ).oBringChildren( hiddenFields, 1 ).scrollHeight ) } )();

      // Reauthorization Queue

        ( function()
            { /* Identificadores */

              const reauthorizationQueue = document.getElementById( "reauthorization-queue" );

              reauthorizationQueue:
                { var boxContainer = reauthorizationQueue.box = reauthorizationQueue.querySelector( ".box-container" );
                  boxContainer:
                    { var heading = boxContainer.heading = boxContainer.querySelector( ".heading-container" ),
                          contentBody = boxContainer.body = boxContainer.querySelector( ".content-body" );
                      heading:
                        { var headingArrow = heading.arrow = heading.querySelector( ".arrow-container" ) }
                      contentBody:
                        { var queueList = contentBody.queueList = reauthorizationQueue.querySelector( ".queue-list" ) } } }

              /* Instanciações */

              HeightSlider1:
                { new HeightSlider1( contentBody, boxContainer, headingArrow, { overflow: true } ) } } )() } )();
