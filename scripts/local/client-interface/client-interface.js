( function()
    { /* Construtores */

        /*---- Editable Content 1 ----*/

        const EditableContent1 = function( sectionContainer, buttonsSet = { edit: false, cancel: false, save: false }, editableData, dataContainers,
                                                featuresMap = { setWidth: false } )
        { assignReferrers:
            { this.sectionContainer = sectionContainer; this.buttonsSet = buttonsSet; this.editableData; this.dataContainers = dataContainers; this.featuresMap = featuresMap;
              buttonsSet:
                { var editButton = buttonsSet.edit, cancelButton = buttonsSet.cancel, saveButton = buttonsSet.save }
              data:
                { var generalData = [], inputData = [];
                  for( let data of editableData ) data.tagName == "INPUT" ? inputData.push( data ): generalData.push( data ) } }
          assignEvents:
            { buttonsSet:
                { let activeEditMode = this.activeEditMode.bind( this, editButton, sectionContainer, editableData, dataContainers, inputData ),
                      undoEditMode = this.undoEditMode.bind( this, cancelButton, sectionContainer, editableData, generalData, inputData ),
                      finishEditMode = this.finishEditMode.bind( this, saveButton, sectionContainer, editableData, inputData ),
                      buttonsMap = new Map( [ [ editButton, activeEditMode ], [ cancelButton, undoEditMode ], [ saveButton, finishEditMode ] ] );
                  for( let [ button, handler ] of buttonsMap ) button.addEventListener( "click", handler ) }
              editableData:
                { for( let data of editableData )
                    { let saveOriginalText = this.saveOriginalText.bind( this, data ),
                          disableLineBreak = this.disableLineBreak.bind( this, data ),
                          changeFocus = this.changeFocus.bind( this, data, editableData ),
                          eventsMap = new Map( [ [ saveOriginalText, "focus" ], [ disableLineBreak, "keypress" ], [ changeFocus, "keypress" ] ] );
                      for( let [ handler, _event ] of eventsMap ) data.addEventListener( _event, handler ) } }
              featuresMap:
                { setWidth:
                    { if( !featuresMap.setWidth ) break setWidth;
                      let resizeDataWidth = this.resizeDataWidth.bind( this, sectionContainer, editableData, dataContainers );
                      window.addEventListener( "resize", resizeDataWidth ) } } } }

        // Chaves do Protótipo

        prototypeKeys:
          { let prototype = EditableContent1.prototype;
            activeEditMode:
              { prototype.activeEditMode = function( editButton, sectionContainer, editableData, dataContainers, inputData, event )
                  { for( let data of editableData ) inputData.includes( data ) ? data.removeAttribute( "disabled" ) : data.setAttribute( "contenteditable", "" );
                    sectionContainer.classList.add( "edit-mode" );
                    if( this.featuresMap.setWidth ) this.resizeDataWidth( sectionContainer, editableData, dataContainers ) } }
            changeContent:
              { prototype.saveOriginalText = function( data, event )
                  { if( "formerText" in data || !data.hasAttribute( "contenteditable" ) ) return;
                    data.formerText = data.innerHTML }
                prototype.disableLineBreak = function( data, event )
                  { if( event.key != "Enter" || event.currentTarget.classList.contains( "text-set" ) ) return;
                    event.preventDefault() }
                prototype.changeFocus = function( data, editableData, event )
                  { if( event.key != "Enter" || ( event.currentTarget.classList.contains( "text-set" ) && !event.shiftKey ) ) return;
                    if( event.currentTarget.classList.contains( "text-set" ) ) event.preventDefault();
                    event.shiftKey ? editableData.oCycle( editableData.indexOf( data ) - 1 ).focus() : editableData.oCycle( editableData.indexOf( data ) + 1 ).focus() } }
            undoEditMode:
              { prototype.undoEditMode = function( cancelButton, sectionContainer, editableData, generalData, inputData, event )
                  { for( let data of generalData ) if( data.formerText && data.formerText != data.innerHTML ) data.innerHTML = data.formerText;
                    return this.finishEditMode( this.buttonsSet.saveButton, sectionContainer, editableData, inputData ) } }
            finishEditMode:
              { prototype.finishEditMode = function( saveButton, sectionContainer, editableData, inputData, event )
                  { for( let data of editableData )
                      { if( "formerText" in data ) delete data.formerText;
                        data.style.width = "";
                        inputData.includes( data ) ? data.setAttribute( "disabled", "" ) : data.removeAttribute( "contenteditable", "" ) }
                    sectionContainer.classList.remove( "edit-mode" ) } }
            adjustDataSize:
              { prototype.resizeDataWidth = function( sectionContainer, editableData, dataContainers, event )
                  { if( !sectionContainer.classList.contains( "edit-mode" ) ) return;
                    for( let data of editableData )
                      { if( data.classList.contains( "text-set" ) ) continue;
                        let containerWidth = window.getComputedStyle( data.oBringParents( dataContainers, 1 ) ).width.replace( /[^0-9.+-]/g, "" );
                        data.style.width = containerWidth / 10 * .98 + "rem" } } } }

      /* Componentes */

        // Intake Summary

        ( function()
            { /* Identificadores */

              const intakeSummary = document.getElementById( "intake-summary" );

              intakeSummary:
                { var sectionBox = intakeSummary.sectionBox = intakeSummary.querySelector( ".box-container" );
                  sectionBox:
                    { var mainHeading = sectionBox.heading = sectionBox.querySelector( ".heading-container" ),
                          mainBody = sectionBox.body = sectionBox.querySelector( ".content-body" ),
                          actionOptions = sectionBox.actionOptions = sectionBox.querySelector( ".action-options" );
                      mainHeading:
                        { var editButton = mainHeading.editButton = mainHeading.querySelector( "[ name$=-edit ]" ),
                              cancelButton = mainHeading.cancelButton = mainHeading.querySelector( "[ name$=-cancel ]" ) }
                      mainBody:
                        { var dataBoxes = mainBody.dataBoxes = Array.from( mainBody.getElementsByClassName( "box" ) ),
                              inquiryReason = mainBody.inquiryReason = mainBody.querySelector( ".inquiry-reason" ),
                              dataContainers = mainBody.dataContainers = dataBoxes.concat( inquiryReason );
                          dataBoxes:
                            { var editableData = dataBoxes.editableData = Array.from( mainBody.getElementsByClassName( "editable" ) );
                              editableData:
                                { var emailData = editableData.email = editableData.find( data => data.closest( ".e-mail" ) ),
                                      emailDataField = dataBoxes.email = emailData.closest( ".e-mail" ) } } } }
                      actionOptions:
                        { var saveButton = actionOptions.saveButton = actionOptions.querySelector( "[ name$=-save ]" ) } }

              /* Instanciações */

              EditableContent1:
                { new EditableContent1( sectionBox, { edit: editButton, save: saveButton, cancel: cancelButton }, editableData, dataContainers, { setWidth: true } ) }

              /* Eventos */

              editableEmailDomainDisplayControl:
                { editButton.addEventListener( "click", placeEmailDomainInEditableArea );
                  saveButton.addEventListener( "click", updateEmailDomain );
                  for( let button of [ cancelButton, saveButton ] ) button.addEventListener( "click", removeEmailDomainOfEditableArea ) }

              /* Funções */

              function placeEmailDomainInEditableArea( event )
                { emailData.insertAdjacentText( "beforeend", emailDataField.querySelector( ".complement" ).textContent ) }

              function updateEmailDomain( event )
                { let newDomain = emailData.textContent.slice( emailData.textContent.lastIndexOf( "@" ) );
                  if( newDomain.startsWith( "@" ) ) emailDataField.querySelector( ".complement" ).textContent = newDomain }

              function removeEmailDomainOfEditableArea( event )
                { let newDomain = emailData.textContent.slice( emailData.textContent.lastIndexOf( "@" ) );
                  if( newDomain.startsWith( "@" ) ) emailData.textContent = emailData.textContent.replace( newDomain, "" ) } } )();

        // Data Tables Columns

        ( function()
            { /* Identificadores */

              const dataTablesColumns = document.getElementById( "data-tables-columns" );

              dataTablesColumns:
                { var sectionBoxes = new oElementsArray( dataTablesColumns.querySelectorAll( ".tables-list > li > .data-table > .box-container" ) );
                  sectionBoxes:
                    { var mainHeadings = sectionBoxes.headings = new oElementsArray( sectionBoxes.oSelectForEach( ".heading-container" ) ),
                          mainBodies = sectionBoxes.bodies = new oElementsArray( sectionBoxes.oSelectForEach( ".content-body" ) ),
                          actionOptions = sectionBoxes.actionOptions = new oElementsArray( sectionBoxes.oSelectForEach( ".action-options" ) );
                      mainHeadings:
                        { var editButtons = mainHeadings.editButtons = mainHeadings.oSelectForEach( "[ name$=-edit ]" ),
                              cancelButtons = mainHeadings.cancelButtons = mainHeadings.oSelectForEach( "[ name$=-cancel ]" ) }
                      mainBodies:
                        { var dataSets = mainBodies.dataSets = new oElementsArray( mainBodies.oSelectForEach( ".data-set", "children", Infinity ) );
                          dataSets:
                            { var dataSetsGroups = dataSets.groups = dataSets.oSegmentByParent( mainBodies ),
                                  editableData = dataSets.editableData = new oElementsArray( dataSets.oSelectForEach( ".editable", "children", Infinity ) );
                              editableData:
                                { var editableDataGroups = editableData.group = editableData.oSegmentByParent( mainBodies ) } } }
                      actionOptions:
                        { var saveButtons = actionOptions.saveButtons = actionOptions.oSelectForEach( "[ name$=-save ]" ) } } }

              /* Instanciações */

              EditableContent1:
                { let editButtonsI = editButtons.values(),
                      cancelButtonsI = cancelButtons.values(),
                      saveButtonsI = saveButtons.values(),
                      editableDataGroupsI = editableDataGroups.values();
                  for( let i = 0; i < sectionBoxes.length; i++ )
                    { if( !sectionBoxes[i].oBringChildren( editButtons, 1 ) ) continue;
                      new EditableContent1( sectionBoxes[i], { edit: editButtonsI.next().value, cancel: cancelButtonsI.next().value, save: saveButtonsI.next().value },
                                            editableDataGroupsI.next().value, dataSetsGroups[i] ) } } } )() } )();
