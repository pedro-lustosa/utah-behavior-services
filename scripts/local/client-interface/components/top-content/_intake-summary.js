/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { EditableContent1 } from "../../../../global/site-presets/referrers/constructors/_editable-content-1.js";

/* Identificadores */

export const intakeSummary = document.getElementById( "intake-summary" );

intakeSummary:
  { var sectionBox = intakeSummary.sectionBox = intakeSummary.querySelector( ".box-container" );
    sectionBox:
      { var mainHeading = sectionBox.heading = sectionBox.querySelector( ".heading-container" ),
            mainBody = sectionBox.body = sectionBox.querySelector( ".content-body" ),
            actionOptions = sectionBox.actionOptions = sectionBox.querySelector( ".action-options" );
        mainHeading:
          { var editButton = mainHeading.editButton = mainHeading.querySelector( "[ name$=-edit ]" ),
                cancelButton = mainHeading.cancelButton = mainHeading.querySelector( "[ name$=-cancel ]" ) }
        mainBody:
          { var dataBoxes = mainBody.dataBoxes = Array.from( mainBody.getElementsByClassName( "box" ) ),
                inquiryReason = mainBody.inquiryReason = mainBody.querySelector( ".inquiry-reason" ),
                dataContainers = mainBody.dataContainers = dataBoxes.concat( inquiryReason );
            dataBoxes:
              { var editableData = dataBoxes.editableData = Array.from( mainBody.getElementsByClassName( "editable" ) );
                editableData:
                  { var emailData = editableData.email = editableData.find( data => data.closest( ".e-mail" ) ),
                        emailDataField = dataBoxes.email = emailData.closest( ".e-mail" ) } } } }
        actionOptions:
          { var saveButton = actionOptions.saveButton = actionOptions.querySelector( "[ name$=-save ]" ) } }

/* Instanciações */

EditableContent1:
  { new EditableContent1( sectionBox, { edit: editButton, save: saveButton, cancel: cancelButton }, editableData, dataContainers, { setWidth: true } ) }

/* Eventos */

editableEmailDomainDisplayControl:
  { editButton.addEventListener( "click", placeEmailDomainInEditableArea );
    saveButton.addEventListener( "click", updateEmailDomain );
    for( let button of [ cancelButton, saveButton ] ) button.addEventListener( "click", removeEmailDomainOfEditableArea ) }

/* Funções */

function placeEmailDomainInEditableArea( event )
  { emailData.insertAdjacentText( "beforeend", emailDataField.querySelector( ".complement" ).textContent ) }

function updateEmailDomain( event )
  { let newDomain = emailData.textContent.slice( emailData.textContent.lastIndexOf( "@" ) );
    if( newDomain.startsWith( "@" ) ) emailDataField.querySelector( ".complement" ).textContent = newDomain }

function removeEmailDomainOfEditableArea( event )
  { let newDomain = emailData.textContent.slice( emailData.textContent.lastIndexOf( "@" ) );
    if( newDomain.startsWith( "@" ) ) emailData.textContent = emailData.textContent.replace( newDomain, "" ) }
