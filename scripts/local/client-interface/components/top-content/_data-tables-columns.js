/* Livrarias */

import { oElementsArray } from "../../../../global/libraries/theWheel/theWheel.js";

/* Construtores */

import { EditableContent1 } from "../../../../global/site-presets/referrers/constructors/_editable-content-1.js";

/* Identificadores */

export const dataTablesColumns = document.getElementById( "data-tables-columns" );

dataTablesColumns:
  { var sectionBoxes = new oElementsArray( dataTablesColumns.querySelectorAll( ".tables-list > li > .data-table > .box-container" ) );
    sectionBoxes:
      { var mainHeadings = sectionBoxes.headings = new oElementsArray( sectionBoxes.oSelectForEach( ".heading-container" ) ),
            mainBodies = sectionBoxes.bodies = new oElementsArray( sectionBoxes.oSelectForEach( ".content-body" ) ),
            actionOptions = sectionBoxes.actionOptions = new oElementsArray( sectionBoxes.oSelectForEach( ".action-options" ) );
        mainHeadings:
          { var editButtons = mainHeadings.editButtons = mainHeadings.oSelectForEach( "[ name$=-edit ]" ),
                cancelButtons = mainHeadings.cancelButtons = mainHeadings.oSelectForEach( "[ name$=-cancel ]" ) }
        mainBodies:
          { var dataSets = mainBodies.dataSets = new oElementsArray( mainBodies.oSelectForEach( ".data-set", "children", Infinity ) );
            dataSets:
              { var dataSetsGroups = dataSets.groups = dataSets.oSegmentByParent( mainBodies ),
                    editableData = dataSets.editableData = new oElementsArray( dataSets.oSelectForEach( ".editable", "children", Infinity ) );
                editableData:
                  { var editableDataGroups = editableData.group = editableData.oSegmentByParent( mainBodies ) } } }
        actionOptions:
          { var saveButtons = actionOptions.saveButtons = actionOptions.oSelectForEach( "[ name$=-save ]" ) } } }

/* Instanciações */

EditableContent1:
  { let editButtonsI = editButtons.values(),
        cancelButtonsI = cancelButtons.values(),
        saveButtonsI = saveButtons.values(),
        editableDataGroupsI = editableDataGroups.values();
    for( let i = 0; i < sectionBoxes.length; i++ )
      { if( !sectionBoxes[i].oBringChildren( editButtons, 1 ) ) continue;
        new EditableContent1( sectionBoxes[i], { edit: editButtonsI.next().value, cancel: cancelButtonsI.next().value, save: saveButtonsI.next().value },
                              editableDataGroupsI.next().value, dataSetsGroups[i] ) } }
