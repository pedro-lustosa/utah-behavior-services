"use strict";

/*---- Livrarias ----*/

import "../../global/libraries/theWheel/theWheel.js";

/*---- Componentes ----*/

import "./components/top-content/_intake-summary.js";

import "./components/top-content/_data-tables-columns.js";
